#!/usr/bin/env python3

import os
import sys
import argparse
from datetime import datetime


# parse the arguments from the call
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--dfile", dest="dfile", help="Data file to parse", required=True)
    parser.add_argument("-d", "--debug", dest="dbg_mode", help="Debug mode, true or false", default=False)
    
    options = parser.parse_args()

    if not options.dfile:
        return parser.error("[-] No data file to parse has been specified.")
    return options


# read the binary file returning the magic number from the global header + data
def file_reader(data):
    if not os.path.exists(data):
        return False
    else:
        try:
            with open(data, 'rb') as content:
                magic_nr = content.read(4)
                # global header is 24 so move other 20 pos
                content.seek(24)
                # the content from now on are the packet-header packet-data sequences
                data = content.read()
            return (magic_nr, data)
        except Exception as e:
            print("[-] Error while reading the file "+str(e))
            return False


# return the byte order (little or big endian)
def byte_order(magic_nr):
    if magic_nr == b'\xd4\xc3\xb2\xa1':
        return 'little'
    elif magic_nr == b'\xa1\xb2\xc3\xd4':
        return 'big'
    else:
        return False


# convert IP addresses from hex to dotted-decimal notation
def readable_ip(hex_ip):
    dec_ip = []
    for el in hex_ip:
        dec_ip.append(str(int(el,16)))
    address = '.'.join(dec_ip)
    return address


# return the packet info for each pack ordering by byte order
def get_pkt_info(barray,bo,pkt_info=[]):
    try:
        # Pkt Time
        pkt_time_utc = barray[0:4][::bo]
        # convert in decimal
        pkt_time_utc = int(''.join(pkt_time_utc),16)
        pkt_timestamp = datetime.utcfromtimestamp(pkt_time_utc).strftime('%d/%m/%Y %H:%M:%S')

        # Pkt saved size (skip the time in us)
        pkt_saved_size = barray[8:12][::bo]
        # convert in decimal 
        pkt_saved_size = int(''.join(pkt_saved_size),16)
        
        # Pkt actual size
        pkt_actual_size = barray[12:16][::bo]
        # convert in decimal 
        pkt_actual_size = int(''.join(pkt_actual_size),16)
        
        # Pkt saved data
        # shift of the heading size (fixed length) while saving the data part whose size is pkt_saved_size
        pkt_saved_data = barray[16:pkt_saved_size+16]
        
        ## From packet saved data get the needed info
        dest_mac = ':'.join(pkt_saved_data[0:6])
        src_mac = ':'.join(pkt_saved_data[6:12])
        pkt_type_hex = ''.join(pkt_saved_data[12:14])
        if pkt_type_hex=='0800':
            # skip some Layer3 header info
            protocol = pkt_saved_data[24]
            src_ip = readable_ip(pkt_saved_data[27:31])
            dest_ip = readable_ip(pkt_saved_data[31:35])
        else: 
            protocol = False
            src_ip = False
            dest_ip = False

        # object to return
        pkt_info.append ({
            "pkt_timestamp": pkt_timestamp,
            "pkt_saved_size": pkt_saved_size,
            "pkt_actual_size": pkt_actual_size,
            "pkt_saved_data": pkt_saved_data,
            "dest_mac": dest_mac,
            "src_mac": src_mac,
            "pkt_type_hex": pkt_type_hex,
            "protocol": protocol,
            "src_ip": src_ip,
            "dest_ip": dest_ip
        })
        
        # if there are still packages, iterate the function on a sub-array, starting from the next one
        # remaining data to analyze =  original minus by the previous packet (header(16) + data)
        if len(barray)>pkt_saved_size+16:
            get_pkt_info(barray[pkt_saved_size+16:],bo,pkt_info)
        return pkt_info
    except Exception as ve:
        print("[-] Error while parsing the packets from the list: "+str(ve))
        return False



if __name__== "__main__":
    opts = get_args()
    #data = file_reader(opts.dfile)
        
    magic_nr, data = file_reader(opts.dfile)
    
    if not data or not magic_nr:
        sys.exit('[-] File to read not found or an error occurred')
    
    bo = byte_order(magic_nr)
    if not bo:
        sys.exit('[-] Not a ".pcap" file')

    # header is 24 + data length
    file_size = len(data)+24
    
    # put the data bytes in a list
    out_hex = ['{:02X}'.format(d) for d in data]
    
    if bo=='little':
        bo = -1
    elif bo=='big':
        bo=1
    
    # get the list of packets with meta-data 
    #print(out_hex) #testing
    target_info = get_pkt_info(out_hex,bo)
    if not target_info:
        sys.exit('[-] Error while parsing the packets from the list. Program quits.')
    
   

    # Printing results
    cntr = 1
    print("Number of packets: "+str(len(target_info)))
    print("File size: "+str(file_size))
    if(opts.dbg_mode):
        print('Byte Order: '+str(bo),' - ".pacp" magic number is:',magic_nr)
    for pkt in target_info:
        print("---"*10)
        print("Packet No.: ",cntr)
        if(opts.dbg_mode):
            print("  ","Packet Timestamp: ",pkt['pkt_timestamp'])
            print("  ","Saved Packet Length: ",pkt['pkt_saved_size'])
            print("  ","Actual Packet Length: ",pkt['pkt_actual_size'])
            print("  ","Packet Data: ")
            ndx = 0
            for byte in pkt['pkt_saved_data']:
                if ndx==15:
                    print("  ",byte)
                    ndx = 0
                else:
                    print("  ",byte,end="")
                    ndx +=1
            print("\n")
            #print("  ",pkt['pkt_saved_data'])
        print("  ","L2 dst MAC address: ",pkt['dest_mac'])
        print("  ","L2 src MAC address: ",pkt['src_mac'])
        print("  ","L2 EtherType value: ",pkt['pkt_type_hex'])
        # print the IP part
        if(pkt['pkt_type_hex']=='0800'):
            print("  ","L3 dst IP address: ",pkt['dest_ip'])
            print("  ","L3 src IP address: ",pkt['src_ip'])
            print("  ","L4 protocol: ",pkt['protocol'])
        # increase the packet counter
        cntr +=1